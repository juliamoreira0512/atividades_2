//Testando operador negação - !

namespace Operador_Nao{

    let possuiDinheiro = false;
    let estaSemDinheiro = !possuiDinheiro;

    console.log(estaSemDinheiro) //true
}