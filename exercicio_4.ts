/*Faça um programa que recebe um número inteiro e verifique se esse número é par ou ímpar.
*/
namespace exercicio_4 {
    let numero, numero_D: number;

    numero = 11;
    numero_D = numero % 2;

    if(numero_D == 1) {
        console.log("O número é ímpar");
    } else {
        console.log("O número é par");
    }
}