/*A nota final de um estudante é calculada a partir de três notas atribuídas respectivamente
a um trabalho de laboratório, a uma avaliação semestral e a um exame final. 
A média das três notas mencionadas anteriormente 
obedece aos pesos 2, 3 e 5 nessa respectiva ordem
 */
namespace exercicio_2 {
  let trabalho_L, avaliacao_S, exame_F, peso_L, peso_S, peso_F, media: number;

  trabalho_L = 8;
  avaliacao_S = 7;
  exame_F = 9;
  peso_L = 2;
  peso_S = 3;
  peso_F = 5;
  media =
    (trabalho_L * peso_L + avaliacao_S * peso_S + exame_F * peso_F) /
    (peso_L + peso_S + peso_F);

  console.log("A média ponderada é:" + media);

  if (media >= 8) {
    console.log("Conceito obtido: A");
  } else if (media >= 7 && media < 8) {
    console.log("Conceito obtido: B");
  } else if (media >= 6 && media < 7) {
    console.log("Conceito obtido: C");
  } else if (media >= 5 && media < 6) {
    console.log("Conceito obtido: D");
  } else {
    console.log("Conceito obtido: E");
  }
}
