namespace exemplo_condicoes {
    let idade: number = 10;
    
    if(idade >= 18)
    {//inicio do bloco
        console.log("Pode dirigir");
    }//fim do bloco
    else
    {
        console.log("Não pode dirigir!");
    }
//ternário
    idade >= 18 ? console.log("Pode dirigir") : console.log("Não pode dirigir!");
}