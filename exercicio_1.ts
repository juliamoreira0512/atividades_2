/*Faça um programa que calcule a média das quatro notas de um aluno 
e informe se ele foi aprovado ou reprovado. 
A nota de corte é de 6 pontos.
*/
namespace exercicio_1 {
    let nota1, nota2, nota3, nota4, media: number;
    nota1 = 7;
    nota2 = 3;
    nota3 = 8;
    nota4 = 9;
    media = (nota1 + nota2 + nota3 + nota4) / 4;

    if(media >= 6)
    {
        console.log("Aluno aprovado, parabéns!!!");
    } else {
        console.log("Aluno reprovado!!!");
    }
}
