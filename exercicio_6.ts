//Crie um algoritmo que solicite três números do usuário e exiba o maior deles.

namespace exercicio_6{
    let numero1, numero2, numero3 : number
    numero1 = 6;
    numero2 = 10;
    numero3 = 25;

    if(numero1 > numero2 && numero1 > numero3) {
        console.log("O maior número é " + numero1);
        //console.log('O número é ${numero1}')
    } else if (numero2 > numero1 && numero2 > numero3) {
        console.log("O maior número é " + numero2);
    } else {
        console.log("O maior número é " + numero3);
    }

}