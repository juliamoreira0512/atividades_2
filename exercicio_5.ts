/*Crie um algoritmo que solicite o nome de um usuário e exiba 
uma mensagem de boas-vindas personalizada de acordo com o 
horário do dia (bom dia, boa tarde ou boa noite).
*/

namespace exercicio_5{
    const data = new Date();
    const horas = data.getHours();

    let name: string
    name = "Julia"


    if (horas >= 3 && horas < 12) {
        console.log ("Bom dia, " + name + ". Seja bem-vindo(a)!")
    } else if (horas >= 12 && horas < 18 ) {
        console.log ("Boa tarde, " + name + ". Seja bem-vindo(a)!")
    } else {
        console.log("Boa noite, " + name + ". Seja bem-vindo(a)!")
    }


}