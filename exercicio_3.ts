/*Faça um programa que receba três números obrigatoriamente em ordem crescente
 e um quarto número que não siga esta regra. Mostre, 
 em seguida, os quatro números em ordem decrescente.
 */
namespace exercicio_3{
    let numero: number;
    
    numero = 6;

    if (numero <= 2) {
        console.log("A ordem decrescente é: 8, 5, 2, " + numero );
    } else if (numero <= 5) {
        console.log("A ordem decrescente é: 8, 5, " + numero + ", 2");
    } else if (numero <= 8) {
        console.log("A ordem decrescente é: 8, " + numero + ", 5, 2" );
    } else
    {
        console.log("A ordem decrescente é:" + numero + ", 8, 5, 2");
    }
}